//
//  ViewController.m
//  TableView
//
//  Created by Nyta on 5/4/21.
//

#import "ViewController.h"
#import "TableViewCell.h"

@interface ViewController ()

@end

@implementation ViewController

   NSArray *tableData;
   NSMutableArray * imagesArray;
   

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTable];
    [self createArray];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [tableData count];;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"cell"];
    
    //Display Data on Cell
    cell.textLabel.text     = [tableData objectAtIndex:indexPath.row];
    cell.imageView.image    = [imagesArray objectAtIndex:indexPath.row];
    cell.lblTitle.text      = @"";
    
    //Configure Cell Item
    cell.btnSwitch.hidden = YES;
    
    //Set tint color of image in button
    UIImage *image = [[UIImage imageNamed:@"next_arrow"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.btnRight setImage:image forState:UIControlStateNormal];
    cell.btnRight.tintColor = [UIColor grayColor];
    
    if (indexPath.row == 0){
        cell.btnSwitch.hidden   = NO;
        cell.btnRight.hidden    = YES;
    }else if (indexPath.row == 1) {
        cell.lblTitle.text      = @"Digi";
    }else if (indexPath.row == 2){
        cell.lblTitle.text      = @"On";
    }else if (indexPath.row == 3){
        cell.lblTitle.text      = @"Off";
    }
    
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self logoutButtonPressed];
}

- (void)logoutButtonPressed
{
     UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Selected Row"
                                 message:@"You have selected a row!"
                                 preferredStyle:UIAlertControllerStyleAlert];

    //Add Buttons

    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Okay"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];

    //Add your buttons to alert controller
    [alert addAction:noButton];

    [self presentViewController:alert animated:YES completion:nil];
}

- (void)configureTable
{
    self.myTableView.dataSource = self;
    self.myTableView.delegate   = self;
    [_myTableView setShowsVerticalScrollIndicator:NO];
    self.navigationItem.title = @"Setting";
}

- (void)createArray
{
    // Define Array Objects
    tableData = [NSArray arrayWithObjects:@"Airplane Mode", @"Wi-Fi", @"Bluetooth", @"Personal Hotspot", @"Notifications", @"Sounds & Haptics", @"Do Not Disturb", @"Screen Time", @"Control Center", nil];
    
    // Define Array Objects Images
    imagesArray = [NSMutableArray arrayWithObjects: [UIImage imageNamed:@"airplane_mode"], [UIImage imageNamed:@"wifi"],[UIImage imageNamed:@"bluetooth"],[UIImage imageNamed:@"personal_hotspot"],[UIImage imageNamed:@"notifications"],[UIImage imageNamed:@"sounds"],[UIImage imageNamed:@"do_not_disturb"],[UIImage imageNamed:@"screen_time"],[UIImage imageNamed:@"control_center"], nil];
}

@end
