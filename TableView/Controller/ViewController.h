//
//  ViewController.h
//  TableView
//
//  Created by Nyta on 5/4/21.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, getter=isAvaiable) BOOL available;

@end

