//
//  TableViewCell.h
//  TableView
//
//  Created by Nyta on 5/6/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnRight;
@property (strong, nonatomic) IBOutlet UISwitch *btnSwitch;


@end

NS_ASSUME_NONNULL_END
