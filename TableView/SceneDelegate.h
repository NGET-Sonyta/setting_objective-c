//
//  SceneDelegate.h
//  TableView
//
//  Created by Nyta on 5/4/21.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

